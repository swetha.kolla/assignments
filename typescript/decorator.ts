export{}
//Class Decorators
@class_decorator
class class1{
    x:number=2
    constructor(){
        console.log("CLASS1")
    }
    @fun_decorator
    f1(){
        console.log("f1")
    }
} 
@class_decorator
class class2{
    x:number=4
    constructor()
    {
        //console.log(x)
        console.log("CLASS@")
    }
}

function fun_decorator(target,propertyKey,des)
{
    console.log("target"+target)
    console.log("pk="+propertyKey)
    console.log("des="+des)
}
function class_decorator(cst)
{
    
    cst.prototype.x=3
    console.log(cst.prototype.x)
}
let ob1=new class1()
console.log(ob1.x)
let ob2=new class2()
console.log(ob2.x)
console.log(ob1.x)