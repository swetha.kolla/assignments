var Callable = /** @class */ (function () {
    function Callable() {
    }
    Callable.prototype.call = function () {
        console.log("Call!");
    };
    return Callable;
}());
var Activable = /** @class */ (function () {
    function Activable() {
        this.active = false;
    }
    Activable.prototype.activate = function () {
        this.active = true;
        console.log("Activating…");
    };
    Activable.prototype.deactive = function () {
        this.active = false;
        console.log("Deactivating…");
    };
    return Activable;
}());
var aa = new Activable();
console.log(Object.getOwnPropertyNames(Activable.prototype));
var MyClass = /** @class */ (function () {
    function MyClass() {
    }
    return MyClass;
}());
function applyMixins(derivedCtor, baseCtors) {
    baseCtors.forEach(function (baseCtor) {
        console.log(Object.getOwnPropertyNames(baseCtor));
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(function (name) {
            //console.log("name"+name)
            //console.log("2"+Object.getOwnPropertyDescriptor(baseCtor.prototype, name))
            var descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, name);
            Object.defineProperty(derivedCtor.prototype, name, descriptor);
        });
    });
}
applyMixins(MyClass, [Callable, Activable]);
var o = new MyClass();
o.call();
o.activate();
