var studentCalc;
(function (studentCalc) {
    function AnualFeeCalc(feeAmount, term) {
        return feeAmount * term;
    }
    studentCalc.AnualFeeCalc = AnualFeeCalc;
    var class1 = /** @class */ (function () {
        function class1() {
            console.log("CONSTRUCTOR");
        }
        return class1;
    }());
    studentCalc.class1 = class1;
})(studentCalc || (studentCalc = {}));
/// <reference path = "./finterface.ts" />  
var TotalFee = studentCalc.AnualFeeCalc(1500, 4);
var class_ob = new studentCalc.class1();
console.log("Output: " + TotalFee);
