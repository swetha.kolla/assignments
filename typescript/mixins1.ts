type Constructor<T = {}>=new (...args: any[]) => T;

function rollno<TBase extends Constructor>(Base: TBase) {
    return class extends Base{
        roll_number=156;
    };
}

function marks<TBase extends Constructor>(Base: TBase) {
    return class extends Base{
        stu_marks=90;
    };
}

class user
{
    name="chandra";
}
const rno=rollno(user);
const mks=rollno(marks(user));

const Nrno=new rno();
console.log(Nrno.name);
console.log(Nrno.roll_number);

const Nmks=new mks();
console.log(Nmks.name)
console.log(Nmks.roll_number);
console.log(Nmks.stu_marks); 