var iterable = [1, 2, 3, 4];
function newIterator(array) {
    var count = 0;
    var result;
    var itr = {
        next: function () {
            result = (count < array.length) ? { value: array[count++], done: false } : { value: undefined, done: true };
            return (result);
        }
    };
    return itr;
}
var iterator = newIterator(iterable);
console.log(iterator);
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
