"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
//Class Decorators
var class1 = /** @class */ (function () {
    function class1() {
        this.x = 2;
        console.log("CLASS1");
    }
    class1.prototype.f1 = function () {
        console.log("f1");
    };
    __decorate([
        fun_decorator
    ], class1.prototype, "f1");
    class1 = __decorate([
        class_decorator
    ], class1);
    return class1;
}());
var class2 = /** @class */ (function () {
    function class2() {
        this.x = 4;
        //console.log(x)
        console.log("CLASS@");
    }
    class2 = __decorate([
        class_decorator
    ], class2);
    return class2;
}());
function fun_decorator(target, propertyKey, des) {
    console.log("target" + target);
    console.log("pk=" + propertyKey);
    console.log("des=" + des);
}
function class_decorator(cst) {
    cst.prototype.x = 3;
    console.log(cst.prototype.x);
}
var ob1 = new class1();
console.log(ob1.x);
var ob2 = new class2();
console.log(ob2.x);
console.log(ob1.x);
