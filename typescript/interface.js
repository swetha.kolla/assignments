var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var kv1 = { key: 1, value: "Steve" }; // OK
var class1 = /** @class */ (function () {
    function class1() {
        console.log("class1");
    }
    return class1;
}());
var class12 = /** @class */ (function (_super) {
    __extends(class12, _super);
    function class12() {
        var _this = _super.call(this) || this;
        console.log("class2");
        return _this;
    }
    return class12;
}(class1));
var ob = new class12();
console.log(n1);
