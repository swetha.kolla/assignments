interface KeyPair {
    key: number;
    value: string;
}

let kv1: KeyPair = { key:1, value:"Steve" }; // OK

class class1
{
    constructor()
    
    {
        console.log("class1")
    }
}
class class12 extends class1
{
    constructor()
    {
        super()
        console.log("class2")
    }
}
var ob=new class12()
