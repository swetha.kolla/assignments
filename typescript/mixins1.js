var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
function rollno(Base) {
    return /** @class */ (function (_super) {
        __extends(class_1, _super);
        function class_1() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.roll_number = 156;
            return _this;
        }
        return class_1;
    }(Base));
}
function marks(Base) {
    return /** @class */ (function (_super) {
        __extends(class_2, _super);
        function class_2() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.stu_marks = 90;
            return _this;
        }
        return class_2;
    }(Base));
}
var user = /** @class */ (function () {
    function user() {
        this.name = "chandra";
    }
    return user;
}());
var rno = rollno(user);
var mks = rollno(marks(user));
var Nrno = new rno();
console.log(Nrno.name);
console.log(Nrno.roll_number);
var Nmks = new mks();
console.log(Nmks.name);
console.log(Nmks.roll_number);
console.log(Nmks.stu_marks);
