// interface f1
// {
//     n:number
// }
// interface f2
// {
//     add(x,y)
// }
// class class1 implements f1,f2
// {
//     n:number
//     add(x,y)
//     {
//         console.log(x+y)
//     }
//     constructor(n){
//         this.n=n+1
//         console.log("Im a constructor in class",this.n)
//     }
// }
// let ob=new class1(5)
// ob.add(2,3)
var class2 = /** @class */ (function () {
    function class2() {
        console.log("Im constructor in class2");
    }
    class2.prototype.sub = function (x, y) {
        console.log(x - y);
    };
    return class2;
}());
var class3 = /** @class */ (function () {
    function class3(x, y) {
        this.x1 = x;
        this.y1 = y;
        console.log(this.x1, this.y1);
    }
    class3.prototype.sub = function (x, y) {
        console.log(x + y);
    };
    return class3;
}());
var ob1 = new class3(4, 5);
ob1.sub(2, 3);
console.log(n1);
