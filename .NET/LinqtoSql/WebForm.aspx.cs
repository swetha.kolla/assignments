﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LinqtoSql
{
    public partial class WebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Delete_Click(object sender, EventArgs e)
        {
           
                connectionDataContext dbContext = new connectionDataContext();
                Employee deleteEmp = dbContext.Employees.SingleOrDefault(emp => emp.EmployeeId == 1213);
                dbContext.Employees.DeleteOnSubmit(deleteEmp);
                dbContext.SubmitChanges();
                message.Text = "Deleted successfully";
            
        }

        protected void Insert_Click(object sender, EventArgs e)
        {
           
                connectionDataContext dbContext = new connectionDataContext();
                Employee emp = new Employee
                {
                    EmployeeId = 1212,
                    Name = "c#",
                    Email = "xyz@gmail.com",
                    ContanctNo = "123456789",
                    DepartmentId = 1,
                    Address = "xyz,colony,hyd"
                };
                dbContext.Employees.InsertOnSubmit(emp);
                dbContext.SubmitChanges();
                message.Text = "Inserted Successfully";
            
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            
                connectionDataContext dbContext = new connectionDataContext();
                Employee updateEmp = dbContext.Employees.SingleOrDefault(emp => emp.EmployeeId == 1212);
                updateEmp.Name = ".net";
                dbContext.SubmitChanges();
                message.Text = "Updated successfully";
            
        }
    }
}