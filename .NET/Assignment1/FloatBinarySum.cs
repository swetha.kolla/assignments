﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment1
{
    class FloatBinarySum
    {
        //public static int statVar=5;
        public FloatBinarySum()
        {
            //statVar+=1;
            Console.WriteLine("Float Binary Digits Sum");
            
            
        }
        public void addfloatBinary()
        {
            string sfb1 = Console.ReadLine();
            string sfb2 = Console.ReadLine();
            float sum = floatBinaryToDecimal(sfb1) + floatBinaryToDecimal(sfb2);
            Console.WriteLine(sum);
        }
        public float floatBinaryToDecimal(string fb)
        {
            //statVar+=1;
            int dotIndex = fb.IndexOf('.');
            float tows = 1;
            float integral = 0, fraction = 0;
            float decimalValue;
            for(int i = dotIndex - 1; i >= 0; i--)
            {
                integral += (fb[i] - '0') * tows;
                tows *= 2;
            }
            tows = 2;
            for(int i = dotIndex + 1; i < fb.Length; i++)
            {
                fraction+= (fb[i] - '0') / tows;
                tows *= 2;
            }
            decimalValue = integral + fraction;
            return decimalValue;
        }

    }
}
