﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment1
{
    class objectClass
    {
        public objectClass()
        {
            Console.WriteLine("object Class");
            var s1 = "Blue";
            object ob = s1;
            var sb = new StringBuilder("Bl");
            sb.Append("ue");
            var s2 = sb.ToString();
            Console.WriteLine(ob == s2);
            Console.WriteLine(s2.Equals(ob));
            Console.WriteLine(object.ReferenceEquals(s1, s2));
            Console.WriteLine(object.Equals(s2, ob));
        }
    }
}
