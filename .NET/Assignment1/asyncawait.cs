﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class asyncawait
    {
        public asyncawait()
        {
            Console.WriteLine("Asyn Await console application");
        }
        public async Task<int> returnCount()
        {
            int count=0;
            for(int i = 0; i < 10; i++)
            {
                count += 2;
            }
            return count;
        }
        public async Task callCount()
        {
            Console.WriteLine("In callCount");
            int count = await returnCount();
            Console.WriteLine(count);
        }
    }
}
