﻿using System;
using System.Threading;

namespace Assignment1
{
    class outWithMultiThreading
    {
        public void invoke1()
        {
            int r;
            Add(out r);
            Console.WriteLine(r);
        }
        public void Add(out int result)
        {
            for (int J = 0; J <= 10; J++)
            {
                Console.WriteLine("Method1 is : {0}", J);
            }
            result = 10;
        }

        public void invoke2()
        {
            int r;
            Add1(out r);
            Console.WriteLine(r);
        }
        public void Add1(out int result)
        {
            for (int J = 0; J <= 10; J++)
            {
                Console.WriteLine("Method2 is : {0}", J);
            }
            result = 20;
        }
    }
}
