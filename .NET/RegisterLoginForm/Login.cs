﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace RegisterLoginForm
{
    public partial class Login : Form
    {
        SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\swetha.kolla\Documents\Employee.mdf;Integrated Security=True;Connect Timeout=30");
        public Login()
        {
            InitializeComponent();
        }

        private void login_button_click(object sender, EventArgs e)
        {
            string query = "select * from Login where username='" + UserNameField.Text + "' and password='" + PasswordField.Text+"'";
            conn.Open();
            SqlCommand sc = new SqlCommand(query, conn);
            SqlDataReader sdr = sc.ExecuteReader();
            if (sdr.Read())
            {
                MessageBox.Show("Login successful");
            }
            else
            {
                MessageBox.Show("Login Failed");
            }
            conn.Close();

        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            /*string query = "insert into Login (username,password) values ('" + UserNameField.Text + "','" + PasswordField.Text + "');";
            conn.Open();
            SqlCommand sc = new SqlCommand(query, conn);
            int status = Convert.ToInt32(sc.ExecuteNonQuery());
            if (status == 1)
            {
                MessageBox.Show("Registered Successfully");
            }
            else
            {
                MessageBox.Show("Registeration failed");
            }*/
            registerdata(UserNameField.Text,PasswordField.Text);
            conn.Close();
        }

        private void registerdata(string uname,string pwd)
        {
            string query = "insert into Login values(@uname,@pwd)";
            conn.Open();
            SqlCommand sc = new SqlCommand(query, conn);
            sc.Parameters.AddWithValue("@uname", uname);
            sc.Parameters.AddWithValue("@pwd", pwd);
            int status = Convert.ToInt32(sc.ExecuteNonQuery());
            if (status == 1)
            {
                MessageBox.Show("Registered Successfully");
            }
            else
            {
                MessageBox.Show("Registeration failed");
            }

        }

        private void forgetpswd_Click(object sender, EventArgs e)
        {

            //string query = "update Login set password='"+PasswordField.Text+"' where username='"+UserNameField.Text+"';";
            conn.Open();
            SqlCommand sc = new SqlCommand("UpdateData", conn) { 
            CommandType=CommandType.StoredProcedure};

            //1st way
            SqlParameter usernameParam = new SqlParameter
            {
                ParameterName = "@username", 
                SqlDbType = SqlDbType.VarChar, 
                Value = UserNameField.Text,
                Direction = ParameterDirection.Input 
            };
            sc.Parameters.Add(usernameParam);
            //2nd way
            sc.Parameters.AddWithValue("@password", PasswordField.Text);
            int status = Convert.ToInt32(sc.ExecuteNonQuery());
            if (status == 1)
            {
                MessageBox.Show("Updated Successfully");
            }
            else
            {
                MessageBox.Show("Updation failed");
            }
            conn.Close();
        }
    }
}
