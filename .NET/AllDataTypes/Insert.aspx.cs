﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AllDataTypes
{
    public partial class Insert : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void InsertBtn_Click(object sender, EventArgs e)
        {
            using(var db=new AllDataTypesEntities())
            {
                var insertTable = new Table { 
                    Id=1,
                    Salary=5000,
                    Name="swetha",
                    JoiningDate=Convert.ToDateTime("17/12/2015"),
                    Age=21,
                    Gender="F",
                    Experience=Convert.ToDecimal(32.4),
                    PhoneNo=987654321                    
                };
                db.Tables.Add(insertTable);
                db.SaveChanges();


            }
        }
    }
}